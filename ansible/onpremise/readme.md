ansible-playbook -t ping setup-host/main.yml -i ./hosts --ask-pass

ansible-playbook -t clean setup-host/main.yml -i ./hosts --limit '!control' --ask-pass

ansible-playbook -t create,install setup-host/main.yml -i ./hosts --limit '!control' --ask-pass

------------------------------------------------------------------------------

ansible-playbook -t install glusterfs/main.yml -i ./hosts --ask-pass --ask-become-pass

ansible-playbook -t init glusterfs/main.yml -i ./hosts --ask-pass --ask-become-pass

------------------------------------------------------------------------------

ansible-playbook -t install cluster-k8s/main.yml -i ./hosts --ask-pass --ask-become-pass

ansible-playbook -t init cluster-k8s/main.yml -i ./hosts --ask-pass --ask-become-pass

-- GET TOKEN => kubeadm token create --print-join-command

-- add token and hash in 'vars/secrets-join'

ansible-playbook -t join cluster-k8s/main.yml -i ./hosts --ask-pass --ask-become-pass

------------------------------------------------------------------------------

ansible-playbook -t install nfs-to-k8s-cluster/main.yml -i ./hosts --ask-pass --ask-become-pass

ansible-playbook -t init nfs-to-k8s-cluster/main.yml -i ./hosts --ask-pass --ask-become-pass

showmount -e IP => nos nodes para ver ser funcionou

-- quando der erro nos nodes com o wave net - tentando buscar im ip 10.96.0.0
1. executar nos nodes:
sudo ip route add 10.96.0.0/16 dev enp0s8 src 172.28.128.4 (IP_NODE)
sudo systemctl restart kubelet

ou 
kubectl get svc
2. sudo route add (IP_CLUSTER) gw (IP_NODE)

------------------------------------------------------------------------------

ansible-playbook -t install monitoring/main.yml -i ./hosts --ask-pass --ask-become-pass

in 'helpers/prometheus-pvc.yml' set IP k8s-master to use in NFS

ansible-playbook -t prometheus monitoring/main.yml -i ./hosts --ask-pass --ask-become-pass

ansible-playbook -t grafana monitoring/main.yml -i ./hosts --ask-pass --ask-become-pass

-------------------------------------------------------------------------------

kubectl describe nodes k8s-node-01 | grep -i taint
Taints:             node-role.kubernetes.io/master:NoSchedule

kubectl describe nodes  | grep -i taints
kubectl taint node k8s-node-01 node-role.kubernetes.io/master:NoSchedule-
kubectl taint node k8s-node-01 node-role.kubernetes.io/master:NoSchedule
kubectl taint node k8s-node-02 key1=value1:NoSchedule
kubectl taint node k8s-node-02 key1=value1:NoSchedule-
kubectl get pods -o wide
kubectl taint node k8s-node-02 key1=value1:NoExecute-
kubectl taint node k8s-node-02 key1=value1:NoExecute
kubectl get pods -o wide
kubectl taint node all key1=value1:NoExecute