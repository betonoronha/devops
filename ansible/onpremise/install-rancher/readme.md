- name: Install docker - by rancher
  shell: |
    curl https://releases.rancher.com/install-docker/19.03.sh | sh
    usermod -aG docker $USER
    
docker run -d --name rancher-server --restart=unless-stopped -v /home/centos/rancher:/opt/rancher --privileged -p 80:80 -p 443:443 rancher/rancher:v2.5.5