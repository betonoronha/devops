instalar cluster K8S:

# apt-get update -y && apt-get upgrade -y

# curl -fsSL https://get.docker.com | bash

# cat > /etc/docker/daemon.json <<EOF
{
    "exec-opts": ["native.cgroupdriver=systemd"],
    "log-driver": "json-file",
    "log-opts": {
        "max-size": "100m"
    },
    "storage-driver": "overlay2"
}
EOF

# mkdir -p /etc/systemd/system/docker.service.d

# systemctl daemon-reload

# systemctl restart docker

# apt-get update && apt-get install -y apt-transport-https

# curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

# echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list

# apt-get update

# apt-get install -y kubelet kubeadm kubectl

# sudo swapoff -a

MASTER:

# kubeadm config images pull
# kubeadm init --apiserver-advertise-address {{ K8S_MASTER_NODE_IP }}

# mkdir -p $HOME/.kube
# sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
# sudo chown $(id -u):$(id -g) $HOME/.kube/config

## USAR O WAVE NET
# kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

### GET TOKEN para rodar nos NODES
#kubeadm token create --print-join-command

NODES:

kubeadm join 127.0.0.1:6443 --token xxx --discovery-token-ca-cert-hash sha256:YYY


### STATUS

# kubectl get nodes
# kubectl get pods --all-namespaces -o wide

###  Start one deploy with nginx

# kubectl create deployment nginx --image=nginx
# kubeclt get deploy nginx
# kubectl get deploy nginx -o yaml > deploy_nginx.yaml

##### DELETE CLUSTER

kubectl config get-cluster
kubectl config delete-cluster kubernetes
apt-get remove -y kubelet kubeadm kubectl

apt-get remove -y docker-ce docker-ce-cli docker-ce-rootless-extras && apt-get autoremove
